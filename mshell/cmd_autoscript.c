/*
 * (C) Copyright 2001
 * Kyle Harris, kharris@nexus-tech.net
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * autoscript allows a remote host to download a command file and,
 * optionally, binary data for automatically updating the target. For
 * example, you create a new kernel image and want the user to be
 * able to simply download the image and the machine does the rest.
 * The kernel image is postprocessed with mkimage, which creates an
 * image with a script file prepended. If enabled, autoscript will
 * verify the script and contents of the download and execute the
 * script portion. This would be responsible for erasing flash,
 * copying the new image, and rebooting the machine.
 */

/* #define DEBUG */

#include <common.h>
#include <command.h>
#include <malloc.h>
#include <asm/byteorder.h>
#ifdef CONFIG_SYS_HUSH_PARSER
#include <hush.h>
#endif
#include <sys/stat.h>
#include <unistd.h>


int
autoscript (const char *script)
{
	ulong		len;
	ulong		*data;
	char		*cmd;
	struct stat st;
	int		rcode = 0;
	FILE * f;
	int debugging=0;
	if( access(script, F_OK ) == -1 ){
		printf("access %s failed\n",script);
		return -1;
	}

	debugging=getenv_yesno("debugging");

	stat(script,&st);
	len = st.st_size;

	if ((cmd = malloc (len + 1)) == NULL) {
		return 1;
	}


	f = fopen(script,"rb");
	rcode=fread(cmd,len,1,f);				
	fclose(f);	

	/* make sure cmd is null terminated */
	*(cmd + len) = 0;

	if(debugging)
		dumpbuffer("autoscr",cmd,len);

#ifdef CONFIG_SYS_HUSH_PARSER /*?? */
	rcode = parse_string_outer (cmd, FLAG_PARSE_SEMICOLON);
#else
	{
		char *line = cmd;
		char *next = cmd;

		/*
		 * break into individual lines,
		 * and execute each line;
		 * terminate on error.
		 */
		while (*next) {
			if (*next == '\n') {
				*next = '\0';
				/* run only non-empty commands */
				if (*line) {
					if(debugging){
					printf ("** exec: \"%s\"\n",
						line);
					}
					if (run_command (line, 0) == -1) {
						rcode = 1;
						break;
					}
				}
				line = next + 1;
			}
			++next;
		}
		if (rcode == 0 && *line)
			rcode = (run_command(line, 0) != -1);
	}
#endif
	free (cmd);
	return rcode;
}

/**************************************************/
#if defined(CONFIG_CMD_AUTOSCRIPT)
int
do_autoscript (cmd_tbl_t *cmdtp, int flag, int argc, char *const argv[])
{
	ulong addr;
	int rcode=0;
	const char *script = NULL;

	/* Find script image */
	if (argc < 2) {
		cmd_usage(cmdtp);
	} else {
		script=argv[1];
		printf ("*  autoscr: script file = %s\n", script);
		rcode = autoscript (script);
	}

	return rcode;
}

U_BOOT_CMD(
	autoscr, 2, 0,	do_autoscript,
	"autoscr - run script from file",
	"<script> - run script from script file"
);
#endif
