/*
 * (C) Copyright 2000-2010
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * (C) Copyright 2001 Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Andreas Heppel <aheppel@sysgo.de>

 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * Support for persistent environment data
 *
 * The "environment" is stored on external storage as a list of '\0'
 * terminated "name=value" strings. The end of the list is marked by
 * a double '\0'. The environment is preceeded by a 32 bit CRC over
 * the data part and, in case of redundant environment, a byte of
 * flags.
 *
 * This linearized representation will also be used before
 * relocation, i. e. as long as we don't have a full C runtime
 * environment. After that, we use a hash table.
 */

#include <common.h>
#include <command.h>
#include <environment.h>
#include <search.h>
#include <errno.h>
#include <linux/stddef.h>
#include <asm/byteorder.h>

#define XMK_STR(x)	#x
#define MK_STR(x)	XMK_STR(x)

/*
 * Maximum expected input data size for import command
 */
#define	MAX_ENV_SIZE	(1 << 20)	/* 1 MiB */


/*
 * This variable is incremented on each do_env_set(), so it can
 * be used via get_env_id() as an indication, if the environment
 * has changed or not. So it is possible to reread an environment
 * variable only if the environment was changed ... done so for
 * example in NetInitLoop()
 */
static int env_id = 1;

int get_env_id (void)
{
	return env_id;
}

/*
 * Command interface: print one or all environment variables
 *
 * Returns 0 in case of error, or length of printed string
 */
static int env_print(char *name)
{
	char *res = NULL;
	size_t len;

	if (name) {		/* print a single name */
		ENTRY e, *ep;

		e.key = name;
		e.data = NULL;
		ep = hsearch (e, FIND);
		if (ep == NULL)
			return 0;
		len = printf ("%s=%s\n", ep->key, ep->data);
		return len;
	}

	/* print whole list */
	len = hexport('\n', &res, 0);

	if (len > 0) {
		puts(res);
		free(res);
		return len;
	}

	/* should never happen */
	return 0;
}

int do_env_print (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int i;
	int rcode = 0;

	if (argc == 1) {
		/* print all env vars */
		rcode = env_print(NULL);
		if (!rcode)
			return 1;
		printf("\nEnvironment size: %d/%ld bytes\n",
			rcode, (ulong)ENV_SIZE);
		return 0;
	}

	/* print selected env vars */
	for (i = 1; i < argc; ++i) {
		int rc = env_print(argv[i]);
		if (!rc) {
			printf("## Error: \"%s\" not defined\n", argv[i]);
			++rcode;
		}
	}

	return rcode;
}

/*
 * Set a new environment variable,
 * or replace or delete an existing one.
 */

int _do_env_set (int flag, int argc, char * const argv[])
{
	int   i, len;
	char  *name, *value, *s;
	ENTRY e, *ep;

	name = argv[1];

	if (strchr(name, '=')) {
		printf ("## Error: illegal character '=' in variable name \"%s\"\n", name);
		return 1;
	}

	env_id++;
	/*
	 * search if variable with this name already exists
	 */
	e.key = name;
	e.data = NULL;
	ep = hsearch (e, FIND);


	/* Delete only ? */
	if ((argc < 3) || argv[2] == NULL) {
		int rc = hdelete(name);
		return !rc;
	}

	/*
	 * Insert / replace new value
	 */
	for (i=2,len=0; i<argc; ++i) {
		len += strlen(argv[i]) + 1;
	}
	if ((value = malloc(len)) == NULL) {
		printf("## Can't malloc %d bytes\n", len);
		return 1;
	}
	for (i=2,s=value; i<argc; ++i) {
		char *v = argv[i];

		while ((*s++ = *v++) != '\0')
			;
		*(s-1) = ' ';
	}
	if (s != value)
		*--s = '\0';

	e.key  = name;
	e.data = value;
	ep = hsearch(e, ENTER);
	free(value);
	if (!ep) {
		printf("## Error inserting \"%s\" variable, errno=%d\n",
			name, errno);
		return 1;
	}

	return 0;
}

int setenvvalue (char *varname, char *varvalue)
{
	char * const argv[4] = { "setenv", varname, varvalue, NULL };
	if ((varvalue == NULL) || (varvalue[0] == '\0'))
		return _do_env_set(0, 2, argv);
	else
		return _do_env_set(0, 3, argv);
}

int do_env_set (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	if (argc < 2)
		return cmd_usage(cmdtp);

	return _do_env_set(flag, argc, argv);
}


/*
 * Interactively edit an environment variable
 */

/*
 * Look up variable from environment,
 * return address of storage for that variable,
 * or NULL if not found
 */
char *getenvvalue (char *name)
{
	if (gd->flags & GD_FLG_ENV_READY) {	/* after import into hashtable */
		ENTRY e, *ep;


		e.key  = name;
		e.data = NULL;
		ep = hsearch (e, FIND);

		return (ep ? ep->data : NULL);
	}

	/* restricted capabilities before import */

	if (getenv_f(name, (char *)(gd->env_buf), sizeof(gd->env_buf)) > 0)
		return (char *)(gd->env_buf);

	return NULL;
}

/*
 * Look up variable from environment for restricted C runtime env.
 */
int getenv_f (char *name, char *buf, unsigned len)
{
	int i, nxt;

	for (i=0; env_get_char(i) != '\0'; i=nxt+1) {
		int val, n;

		for (nxt=i; env_get_char(nxt) != '\0'; ++nxt) {
			if (nxt >= CONFIG_ENV_SIZE) {
				return (-1);
			}
		}
		if ((val=envmatch((uchar *)name, i)) < 0)
			continue;

		/* found; copy out */
		for (n=0; n<len; ++n, ++buf) {
			if ((*buf = env_get_char(val++)) == '\0')
				return n;
		}

		if (n)
			*--buf = '\0';

		printf("env_buf too small [%d]\n", len);

		return n;
	}
	return (-1);
}


int do_env_save (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	extern char * env_name_spec;

	printf ("Saving Environment to %s...\n", env_name_spec);

	return (saveenv() ? 1 : 0);
}

U_BOOT_CMD(
	saveenv, 1, 0,	do_env_save,
	"save environment variables to persistent storage",
	""
);



/*
 * Match a name / name=value pair
 *
 * s1 is either a simple 'name', or a 'name=value' pair.
 * i2 is the environment index for a 'name2=value2' pair.
 * If the names match, return the index for the value2, else NULL.
 */

int envmatch (uchar *s1, int i2)
{

	while (*s1 == env_get_char(i2++))
		if (*s1++ == '=')
			return(i2);
	if (*s1 == '\0' && env_get_char(i2-1) == '=')
		return(i2);
	return(-1);
}

static int do_env_default(cmd_tbl_t * cmdtp, int flag, int argc, char * const argv[])
{
	if ((argc != 2) || (strcmp(argv[1], "-f") != 0)) {
		cmd_usage(cmdtp);
		return 1;
	}
	set_default_env("## Resetting to default environment\n");
	return 0;
}

static int do_env_delete(cmd_tbl_t * cmdtp, int flag, int argc, char * const argv[])
{
	printf("Not implemented yet\n");
	return 0;
}

/*
 * env export [-t | -b | -c] addr [size]
 *	-t:	export as text format; if size is given, data will be
 *		padded with '\0' bytes; if not, one terminating '\0'
 *		will be added (which is included in the "filesize"
 *		setting so you can for exmple copy this to flash and
 *		keep the termination).
 *	-b:	export as binary format (name=value pairs separated by
 *		'\0', list end marked by double "\0\0")
 *	-c:	export as checksum protected environment format as
 *		used for example by "saveenv" command
 *	addr:	memory address where environment gets stored
 *	size:	size of output buffer
 *
 * With "-c" and size is NOT given, then the export command will
 * format the data as currently used for the persistent storage,
 * i. e. it will use CONFIG_ENV_SECT_SIZE as output block size and
 * prepend a valid CRC32 checksum and, in case of resundant
 * environment, a "current" redundancy flag. If size is given, this
 * value will be used instead of CONFIG_ENV_SECT_SIZE; again, CRC32
 * checksum and redundancy flag will be inserted.
 *
 * With "-b" and "-t", always only the real data (including a
 * terminating '\0' byte) will be written; here the optional size
 * argument will be used to make sure not to overflow the user
 * provided buffer; the command will abort if the size is not
 * sufficient. Any remainign space will be '\0' padded.
 *
 * On successful return, the variable "filesize" will be set.
 * Note that filesize includes the trailing/terminating '\0' byte(s).
 *
 * Usage szenario:  create a text snapshot/backup of the current settings:
 *
 *	=> env export -t 100000
 *	=> era ${backup_addr} +${filesize}
 *	=> cp.b 100000 ${backup_addr} ${filesize}
 *
 * Re-import this snapshot, deleting all other settings:
 *
 *	=> env import -d -t ${backup_addr}
 */
static int do_env_export(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	char	buf[32];
	char	*addr, *cmd, *res;
	size_t	size;
	ssize_t	len;
	env_t	*envp;
	char	sep = '\n';
	int	chk = 0;
	int	fmt = 0;

	cmd = *argv;

	while (--argc > 0 && **++argv == '-') {
		char *arg = *argv;
		while (*++arg) {
			switch (*arg) {
			case 'b':		/* raw binary format */
				if (fmt++)
					goto sep_err;
				sep = '\0';
				break;
			case 'c':		/* external checksum format */
				if (fmt++)
					goto sep_err;
				sep = '\0';
				chk = 1;
				break;
			case 't':		/* text format */
				if (fmt++)
					goto sep_err;
				sep = '\n';
				break;
			default:
				cmd_usage(cmdtp);
				return 1;
			}
		}
	}

	if (argc < 1) {
		cmd_usage(cmdtp);
		return 1;
	}

	addr = (char *)simple_strtoul(argv[0], NULL, 16);

	if (argc == 2) {
		size = simple_strtoul(argv[1], NULL, 16);
		memset(addr, '\0', size);
	} else {
		size = 0;
	}

	if (sep) {		/* export as text file */
		len = hexport(sep, &addr, size);
		if (len < 0) {
			printf("Cannot export environment\n");
			return 1;
		}
		sprintf(buf, "%zX", len);
		//setenvvalue("filesize", buf);

		return 0;
	}

	envp = (env_t *)addr;

	if (chk)		/* export as checksum protected block */
		res = (char *)envp->data;
	else			/* export as raw binary data */
		res = addr;

	len = hexport('\0', &res, ENV_SIZE);
	if (len < 0) {
		printf("Cannot export environment\n");
		return 1;
	}

	if (chk) {
		envp->crc   = crc32(0, envp->data, ENV_SIZE);
#ifdef CONFIG_ENV_ADDR_REDUND
		envp->flags = ACTIVE_FLAG;
#endif
	}
	sprintf(buf, "%zX", len + offsetof(env_t,data));
	setenvvalue("filesize", buf);

	return 0;

sep_err:
	printf("## %s: only one of \"-b\", \"-c\" or \"-t\" allowed\n",
		cmd);
	return 1;
}

/*
 * env import [-d] [-t | -b | -c] addr [size]
 *	-d:	delete existing environment before importing;
 *		otherwise overwrite / append to existion definitions
 *	-t:	assume text format; either "size" must be given or the
 *		text data must be '\0' terminated
 *	-b:	assume binary format ('\0' separated, "\0\0" terminated)
 *	-c:	assume checksum protected environment format
 *	addr:	memory address to read from
 *	size:	length of input data; if missing, proper '\0'
 *		termination is mandatory
 */
static int do_env_import(cmd_tbl_t * cmdtp, int flag, int argc, char * const argv[])
{
	char	*cmd, *addr;
	char	sep = '\n';
	int	chk = 0;
	int	fmt = 0;
	int	del = 0;
	size_t	size;

	cmd = *argv;

	while (--argc > 0 && **++argv == '-') {
		char *arg = *argv;
		while (*++arg) {
			switch (*arg) {
			case 'b':		/* raw binary format */
				if (fmt++)
					goto sep_err;
				sep = '\0';
				break;
			case 'c':		/* external checksum format */
				if (fmt++)
					goto sep_err;
				sep = '\0';
				chk = 1;
				break;
			case 't':		/* text format */
				if (fmt++)
					goto sep_err;
				sep = '\n';
				break;
			case 'd':
				del = 1;
				break;
			default:
				cmd_usage(cmdtp);
				return 1;
			}
		}
	}

	if (argc < 1) {
		cmd_usage(cmdtp);
		return 1;
	}

	if (!fmt)
		printf("## Warning: defaulting to text format\n");

	addr = (char *)simple_strtoul(argv[0], NULL, 16);

	if (argc == 2) {
		size = simple_strtoul(argv[1], NULL, 16);
	} else {
		char *s = addr;

		size = 0;

		while (size < MAX_ENV_SIZE) {
			if ((*s == sep) && (*(s+1) == '\0'))
				break;
			++s;
			++size;
		}
		if (size == MAX_ENV_SIZE) {
			printf("## Warning: Input data exceeds %d bytes"
				" - truncated\n", MAX_ENV_SIZE);
		}
		++size;
		printf("## Info: input data size = %zd = 0x%zX\n", size, size);
	}

	if (chk) {
		uint32_t crc;
		env_t *ep = (env_t *)addr;

		size -= offsetof(env_t, data);
		memcpy(&crc, &ep->crc, sizeof(crc));

		if (crc32(0, ep->data, size) != crc) {
			puts("## Error: bad CRC, import failed\n");
			return 1;
		}
		addr = (char *)ep->data;
	}

	if (himport(addr, size, sep, del ? 0 : H_NOCLEAR) == 0) {
		printf("Environment import failed\n");
		return 1;
	}
	gd->flags |= GD_FLG_ENV_READY;

	return 0;

sep_err:
	printf("## %s: only one of \"-b\", \"-c\" or \"-t\" allowed\n",
		cmd);
	return 1;
}


/*
 * New command line interface: "env" command with subcommands
 */
static cmd_tbl_t cmd_env_sub[] = {
	U_BOOT_CMD_MKENT(default, 1, 0, do_env_default, "", ""),
	U_BOOT_CMD_MKENT(delete, 2, 0, do_env_delete, "", ""),
	U_BOOT_CMD_MKENT(export, 4, 0, do_env_export, "", ""),
	U_BOOT_CMD_MKENT(import, 5, 0, do_env_import, "", ""),
	U_BOOT_CMD_MKENT(print, CONFIG_SYS_MAXARGS, 1, do_env_print, "", ""),
	U_BOOT_CMD_MKENT(save, 1, 0, do_env_save, "", ""),
	U_BOOT_CMD_MKENT(set, CONFIG_SYS_MAXARGS, 0, do_env_set, "", ""),
};


static int do_env (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	cmd_tbl_t *cp;

	/* drop initial "env" arg */
	argc--;
	argv++;
	if(argc!=0){
		cp = find_cmd_tbl(argv[0], cmd_env_sub, ARRAY_SIZE(cmd_env_sub));

		if (cp)
			return cp->cmd(cmdtp, flag, argc, argv);
	}
	cmd_usage(cmdtp);
	return 1;
}

U_BOOT_CMD(
	env, CONFIG_SYS_MAXARGS, 1, do_env,
	"environment handling commands",
	"default -f - reset default environment\n"
	"env export [-t | -b | -c] addr [size] - export environmnt\n"
	"env import [-d] [-t | -b | -c] addr [size] - import environmnt\n"
	"env print [name ...] - print environment\n"
	"env save - save environment\n"
	"env set [-f] name [arg ...]\n"
);

/*
 * Old command line interface, kept for compatibility
 */

U_BOOT_CMD(
	printenv, CONFIG_SYS_MAXARGS, 1,	do_env_print,
	"print environment variables",
	"\n    - print values of all environment variables\n"
	"printenv name ...\n"
	"    - print value of environment variable 'name'"
);

U_BOOT_CMD(
	setenv, CONFIG_SYS_MAXARGS, 0,	do_env_set,
	"set environment variables",
	"name value ...\n"
	"    - set environment variable 'name' to 'value ...'\n"
	"setenv name\n"
	"    - delete environment variable 'name'"
);

