#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include "common.h"
#include "command.h"
/* supported commands including solicted and unsolicited commands */
#define     CMD_SUCCESS             	0x00
#define     CMD_FAILED              	-1    
#define     CMD_SET_JPEG_STREAM  		0x01   
#define     CMD_SET_JPEG_STREAM_RSP		0X02   
#define 	CMD_GET_JPEG_STREAM			0x03	
#define 	CMD_GET_JPEG_STREAM_RSP		0x04	
#define     CMD_SET_MOTION_DETECT   	0x10   
#define     CMD_SET_MOTION_DETECT_RSP   0x11   
#define     CMD_GET_MOTION_DETECT       0x12   
#define     CMD_GET_MOTION_DETECT_RSP   0x13   
#define 	CMD_START_JPEG_STREAM		0x70	
#define 	CMD_START_JPEG_STREAM_RSP   0x71	
#define 	CMD_STOP_JPEG_STREAM		0x72	
#define 	CMD_STOP_JPEG_STREAM_RSP	0x73	

#define     CMD_UNSOLICITED_MOTION_WARN 0x20   
#define     CMD_UNSOLICITED_JPEG_DATA	0xA0	


/*
* IPC communication packet format
*---------------------------------------------------------|
*|flag 2B|version 1B|command 1B|payload length 4B| payload|
*---------------------------------------------------------|
*/
#pragma pack(1) 
typedef struct command_header
{  
	unsigned short   flag;
	unsigned char    version;
	unsigned char    command;
	unsigned int     length;
}st_cmd_header_t,*pst_cmd_header_t;
#pragma pack() 
typedef struct jpeg_stream_params_t
{
	/*xfer type,currently only jpeg is supported.
	 * 1:jpeg
	*/
	unsigned char type;
	/*
	 *  0x00 :QCIF 176 X 144
	 *	0x01 : 180p 320 X 180
	 *	0x02 : QVGA 320 X 240
	 *	0x03 : CIF 352 X 288
	 *	0x04 : VGA 640 X 480
	 *	0x05 : D1 720 X 576
	 *	0x06 : 720P 1280 X 720
	 *	0x07 : 960P�� 1280 X 960
	 *	0x08 : UVGA 1600 X 1200
	 *	0x09 : 1080P 1920 X 1080
	 *	if capability of device is 960P in maximum, if requested size is greater than it,it will be degrade to 960P
	*/
	unsigned char size;
	/*requested frame per second 1-30*/
	unsigned char fps;
	/*jpeg quality,0-100,100 is highest quality*/
	unsigned char quality;
	/*reserved field,default is 00*/
	unsigned int reserved;
	
}st_jpeg_stream_params,*pst_jpeg_stream_params;

typedef struct motion_detect_params_t
{

	/*
	 *motion detection enable
	 * 0: disabled
	 * 1: enabled
	*/
	unsigned char enabled;

    /*
     * motion detection threshold
     * 0-100, 100 means highest sensitivity
	*/
	unsigned char threshold;

}st_motion_detect_params,*pst_motion_detect_params;
typedef struct cmd_xfer_resp_t
{
	/*common parts*/
	unsigned short ipc_errno;
	unsigned short next_len;
}st_cmd_xfer_resp_t,*pst_cmd_xfer_resp_t;
typedef struct cmd_xfer_status_t
{	
	st_cmd_xfer_resp_t resp;

	/*
	 * handshake status
	*/
	unsigned char handshake_commnad;

	/*
	 * 0: handshake not completed
	 * 1: handshake completed
	 * handshake_state ->0 is set by sender
	 * handshake_state ->1 is set by receiver
	*/
	unsigned char handshake_state;
}st_cmd_xfer_status_t,*pst_cmd_xfer_status_t;

static st_jpeg_stream_params jpeg_stream_params;
static st_motion_detect_params motion_detect_params;
static st_cmd_xfer_status_t cmd_xfer_status;



static int socket_fd=-1;//ipc sockect fd

static void dump_ipc_cmd_resp_packet(pst_cmd_header_t pch)
{
    printf("------------------------------------\n");
    printf("flag:0x%0x\n",pch->flag);
    printf("ver:0x%0x\n",pch->version);
    if(CMD_SET_JPEG_STREAM_RSP==pch->command)
	printf("command:set jpeg stream\n");
    else if(CMD_GET_JPEG_STREAM_RSP==pch->command)
	printf("command:get jpeg stream\n");
    else if(CMD_SET_MOTION_DETECT_RSP==pch->command)
	printf("command:set motion detect\n");
    else if(CMD_GET_MOTION_DETECT_RSP==pch->command)
	printf("command:get motion detect\n");
    else if(CMD_START_JPEG_STREAM_RSP==pch->command)
	printf("command:start jpeg stream\n");
    else if(CMD_STOP_JPEG_STREAM_RSP==pch->command)
	printf("command:stop jpeg stream\n");
    else if(CMD_UNSOLICITED_MOTION_WARN==pch->command)
	printf("command:motion detected\n");
    else if(CMD_UNSOLICITED_JPEG_DATA==pch->command)
	printf("command:jpeg data\n");
    else
	printf("command:unknown 0x%x\n",pch->command);
    printf("length:0x%0x\n",pch->length);
    printf("----------------------------------\n");
}

static int send_ipc_cmd_packet(pst_cmd_header_t pch,char* payload,int payload_length)
{
	int ret=0;
	if(socket_fd<0) return -ENOENT;
	ret = write(socket_fd,pch,sizeof(st_cmd_header_t));
	if(-1==ret) {
		ret = errno;
		if(ret>0) ret=-ret;
		goto bye;
	}
	if(getenv_noyes("ipc_debug")){
		printf("write expected %d,written:%d\n",sizeof(st_cmd_header_t),ret);
		if(ret>0)
			dumpbuffer(">>",(char*)pch,sizeof(st_cmd_header_t));
	}
	if(payload&&payload_length>0){
		ret=write(socket_fd,payload,payload_length);
		if(-1==ret) {
		  ret = errno;
		  if(ret>0) ret=-ret;
		  goto bye;
		}
		if(getenv_noyes("ipc_debug")){
			printf("write expected %d,written:%d\n",payload_length,ret);
			if(ret>0)
			dumpbuffer(">>",(char*)payload,payload_length);
		}
	}
bye:
	return ret;
}


static int request_jpeg_stream_params(unsigned char format,unsigned char fps,unsigned char quality)
{
	int ret=0;
	int timeout=3000;
	st_cmd_header_t sch={0x55AA,0x01,CMD_SET_JPEG_STREAM,sizeof(st_jpeg_stream_params)};
	st_jpeg_stream_params sjsp={1,format,fps,quality,0};
	//
	//reset cmd_xfer_status
	//
	cmd_xfer_status.handshake_commnad = CMD_SET_JPEG_STREAM;
	cmd_xfer_status.handshake_state = 0;
	ret=send_ipc_cmd_packet(&sch,(char*)&sjsp,sizeof(st_jpeg_stream_params));
	if(ret<=0) goto function_exit;
	while(!cmd_xfer_status.handshake_state&&timeout--){
		usleep(1000);
	}
	if(!cmd_xfer_status.handshake_state){
		printf("timeout to %s\n ",__FUNCTION__);
		ret=-EIO;
	}else{
		if(cmd_xfer_status.resp.ipc_errno){
			printf("ipc response error code=%d @%s\n",cmd_xfer_status.resp.ipc_errno,__FUNCTION__);
			ret=-cmd_xfer_status.resp.ipc_errno;
		}else
		    ret=0;
		
	}
	
function_exit:	
	return ret;
}
static int retrieve_jpeg_stream_params(void)
{
	int ret=0;
	int timeout=3000;
	st_cmd_header_t sch={0x55AA,0x01,CMD_GET_JPEG_STREAM,0x00};
	//
	//reset cmd_xfer_status
	//
	cmd_xfer_status.handshake_commnad = CMD_GET_JPEG_STREAM;
	cmd_xfer_status.handshake_state = 0;
	ret=send_ipc_cmd_packet(&sch,NULL,0);
	if(ret<=0) goto function_exit;
	while(!cmd_xfer_status.handshake_state&&timeout--){
		usleep(1000);
	}
	if(!cmd_xfer_status.handshake_state){
		printf("timeout to %s\n ",__FUNCTION__);
		ret=-EIO;
	}else{
		if(cmd_xfer_status.resp.ipc_errno){
			printf("ipc response error code=%d @%s\n",cmd_xfer_status.resp.ipc_errno,__FUNCTION__);
			ret=-cmd_xfer_status.resp.ipc_errno;
		}else
		    ret=0;
		
	}
	
function_exit:	
	return ret;
}

static int request_motion_detect_params(unsigned char enabled,unsigned char threshold)
{
	int ret=0;
	int timeout=3000;
	st_cmd_header_t sch={0x55AA,0x01,CMD_SET_MOTION_DETECT,sizeof(st_motion_detect_params)};
	st_motion_detect_params smdp={enabled,threshold};
	//
	//reset cmd_xfer_status
	//
	cmd_xfer_status.handshake_commnad = CMD_SET_MOTION_DETECT;
	cmd_xfer_status.handshake_state = 0;
	ret=send_ipc_cmd_packet(&sch,(char*)&smdp,sizeof(st_motion_detect_params));
	if(ret<=0) goto function_exit;
	while(!cmd_xfer_status.handshake_state&&timeout--){
		usleep(1000);
	}
	if(!cmd_xfer_status.handshake_state){
		printf("timeout to %s\n ",__FUNCTION__);
		ret=-EIO;
	}else{
		if(cmd_xfer_status.resp.ipc_errno){
			printf("ipc response error code=%d @%s\n",cmd_xfer_status.resp.ipc_errno,__FUNCTION__);
			ret=-cmd_xfer_status.resp.ipc_errno;
		}else
		    ret=0;
		
	}
function_exit:	
	return ret;
}
static int retrieve_motion_detect_params(void)
{
	int ret=0;
	int timeout=3000;
	st_cmd_header_t sch={0x55AA,0x01,CMD_GET_MOTION_DETECT,0x00};
	//
	//reset cmd_xfer_status
	//
	cmd_xfer_status.handshake_commnad = CMD_GET_MOTION_DETECT;
	cmd_xfer_status.handshake_state = 0;
	ret=send_ipc_cmd_packet(&sch,NULL,0);
	if(ret<=0) goto function_exit;
	while(!cmd_xfer_status.handshake_state&&timeout--){
		usleep(1000);
	}
	if(!cmd_xfer_status.handshake_state){
		printf("timeout to %s\n ",__FUNCTION__);
		ret=-EIO;
	}else{
		if(cmd_xfer_status.resp.ipc_errno){
			printf("ipc response error code=%d @%s\n",cmd_xfer_status.resp.ipc_errno,__FUNCTION__);
			ret=-cmd_xfer_status.resp.ipc_errno;
		}else
		    ret=0;
		
	}
function_exit:	
	return ret;
}

static int request_jpeg_stream_start(void)
{
	int ret=0;
	int timeout=3000;
	st_cmd_header_t sch={0x55AA,0x01,CMD_START_JPEG_STREAM,0x00};
	//
	//reset cmd_xfer_status
	//
	cmd_xfer_status.handshake_commnad = CMD_START_JPEG_STREAM;
	cmd_xfer_status.handshake_state = 0;
	ret=send_ipc_cmd_packet(&sch,NULL,0);
	if(ret<=0) goto function_exit;
	while(!cmd_xfer_status.handshake_state&&timeout--){
		usleep(1000);
	}
	if(!cmd_xfer_status.handshake_state){
		printf("timeout to %s\n ",__FUNCTION__);
		ret=-EIO;
	}else{
		if(cmd_xfer_status.resp.ipc_errno){
			printf("ipc response error code=%d @%s\n",cmd_xfer_status.resp.ipc_errno,__FUNCTION__);
			ret=-cmd_xfer_status.resp.ipc_errno;
		}else
		    ret=0;
		
	}
function_exit:
	return ret;
}

static int request_jpeg_stream_stop(void)
{
	int ret=0;
	int timeout=3000;
	st_cmd_header_t sch={0x55AA,0x01,CMD_STOP_JPEG_STREAM,0x00};
	//
	//reset cmd_xfer_status
	//
	cmd_xfer_status.handshake_commnad = CMD_STOP_JPEG_STREAM;
	cmd_xfer_status.handshake_state = 0;
	ret=send_ipc_cmd_packet(&sch,NULL,0);
	if(ret<=0) goto function_exit;
	while(!cmd_xfer_status.handshake_state&&timeout--){
		usleep(1000);
	}
	if(!cmd_xfer_status.handshake_state){
		printf("timeout to %s\n ",__FUNCTION__);
		ret=-EIO;
	}else{
		if(cmd_xfer_status.resp.ipc_errno){
			printf("ipc response error code=%d @%s\n",cmd_xfer_status.resp.ipc_errno,__FUNCTION__);
			ret=-cmd_xfer_status.resp.ipc_errno;
		}else
		    ret=0;
		
	}
function_exit:
	return ret;
}

/*create client socket*/
static int connect_to_target(const char* target,int port)
{
	int fd;
	int ret = 0;
	int cliaddr_len = 0;
	struct sockaddr_in cliaddr;
	struct hostent *hptr;
	char target_ipaddr[32];
	int target_port=(0==port)?8008:port;
	fd = socket(AF_INET,SOCK_STREAM,0);
	if(fd < 0)
	{
		fprintf(stderr,"client sock ERR!\n");
		return fd;
	}
	if(target&&((hptr = gethostbyname(target)) == NULL)){
	    printf("gethostbyname error for host:%s\n", target);   
	    return -EINVAL;
	}else if(target){
		char** pptr;
		pptr=hptr->h_addr_list;  
		if(NULL!=pptr)
			inet_ntop(hptr->h_addrtype, *pptr, target_ipaddr, sizeof(target_ipaddr));
		else{
			sprintf(target_ipaddr,"%s","192.168.42.118");
		}
	}else{	
		sprintf(target_ipaddr,"%s","192.168.42.118");
	}
        printf("trying to connect %s:%d\n",target_ipaddr,target_port);
	cliaddr.sin_family = AF_INET;
	cliaddr.sin_port = htons(target_port);
	cliaddr.sin_addr.s_addr = inet_addr(target_ipaddr);
	
	cliaddr_len = sizeof(cliaddr);
	
	ret = connect(fd,(struct sockaddr*)&cliaddr,cliaddr_len);
	if(ret < 0)
	{
		fprintf(stderr,"client connect ERR!\n");
		close(fd);
		return -1;
	}
	
	fcntl(fd,F_SETFL,fcntl(fd,F_GETFL,0)|O_NONBLOCK);

	printf("connect to %s:%d okay\n",target_ipaddr,target_port);
	
	return fd;
}

#include <setjmp.h>
#include <jpeglib.h>
struct my_error_mgr {
  struct jpeg_error_mgr pub;	/* "public" fields */

  jmp_buf setjmp_buffer;	/* for return to caller */
};

typedef struct my_error_mgr * my_error_ptr;

/*
 * Here's the routine that will replace the standard error_exit method:
 */

void 
	my_error_exit (j_common_ptr cinfo)
{
  /* cinfo->err really points to a my_error_mgr struct, so coerce pointer */
  my_error_ptr myerr = (my_error_ptr) cinfo->err;

  /* Always display the message. */
  /* We could postpone this until after returning, if we chose. */
  (*cinfo->err->output_message) (cinfo);

  /* Return control to the setjmp point */
  longjmp(myerr->setjmp_buffer, 1);
}

static int jpeg_file_checking(char * filename)
{
  /* This struct contains the JPEG decompression parameters and pointers to
   * working space (which is allocated as needed by the JPEG library).
   */
  struct jpeg_decompress_struct cinfo;
  /* We use our private extension JPEG error handler.
   * Note that this struct must live as long as the main JPEG parameter
   * struct, to avoid dangling-pointer problems.
   */
  struct my_error_mgr jerr;
  /* More stuff */
  FILE * infile;		/* source file */
  JSAMPARRAY buffer;		/* Output row buffer */
  int row_stride;		/* physical row width in output buffer */

  /* In this example we want to open the input file before doing anything else,
   * so that the setjmp() error recovery below can assume the file is open.
   * VERY IMPORTANT: use "b" option to fopen() if you are on a machine that
   * requires it in order to read binary files.
   */

  if ((infile = fopen(filename, "rb")) == NULL) {
    fprintf(stderr, "can't open %s\n", filename);
    return 0;
  }

  /* Step 1: allocate and initialize JPEG decompression object */

  /* We set up the normal JPEG error routines, then override error_exit. */
  cinfo.err = jpeg_std_error(&jerr.pub);
  jerr.pub.error_exit = my_error_exit;
  /* Establish the setjmp return context for my_error_exit to use. */
  if (setjmp(jerr.setjmp_buffer)) {
    /* If we get here, the JPEG code has signaled an error.
     * We need to clean up the JPEG object, close the input file, and return.
     */
    jpeg_destroy_decompress(&cinfo);
    fclose(infile);
    return 0;
  }
  /* Now we can initialize the JPEG decompression object. */
  jpeg_create_decompress(&cinfo);

  /* Step 2: specify data source (eg, a file) */

  jpeg_stdio_src(&cinfo, infile);

  /* Step 3: read file parameters with jpeg_read_header() */

  (void) jpeg_read_header(&cinfo, TRUE);
  /* We can ignore the return value from jpeg_read_header since
   *   (a) suspension is not possible with the stdio data source, and
   *   (b) we passed TRUE to reject a tables-only JPEG file as an error.
   * See libjpeg.doc for more info.
   */

  /* Step 4: set parameters for decompression */

  /* In this example, we don't need to change any of the defaults set by
   * jpeg_read_header(), so we do nothing here.
   */

  /* Step 5: Start decompressor */

  (void) jpeg_start_decompress(&cinfo);
  /* We can ignore the return value since suspension is not possible
   * with the stdio data source.
   */

  /* We may need to do some setup of our own at this point before reading
   * the data.  After jpeg_start_decompress() we have the correct scaled
   * output image dimensions available, as well as the output colormap
   * if we asked for color quantization.
   * In this example, we need to make an output work buffer of the right size.
   */ 
  /* JSAMPLEs per row in output buffer */
  row_stride = cinfo.output_width * cinfo.output_components;
  /* Make a one-row-high sample array that will go away when done with image */
  buffer = (*cinfo.mem->alloc_sarray)
		((j_common_ptr) &cinfo, JPOOL_IMAGE, row_stride, 1);

  /* Step 6: while (scan lines remain to be read) */
  /*           jpeg_read_scanlines(...); */

  /* Here we use the library's state variable cinfo.output_scanline as the
   * loop counter, so that we don't have to keep track ourselves.
   */
  while (cinfo.output_scanline < cinfo.output_height) {
    /* jpeg_read_scanlines expects an array of pointers to scanlines.
     * Here the array is only one element long, but you could ask for
     * more than one scanline at a time if that's more convenient.
     */
    (void) jpeg_read_scanlines(&cinfo, buffer, 1);
    /* Assume put_scanline_someplace wants a pointer and sample count. */
    //put_scanline_someplace(buffer[0], row_stride);
  }

  /* Step 7: Finish decompression */

  (void) jpeg_finish_decompress(&cinfo);
  /* We can ignore the return value since suspension is not possible
   * with the stdio data source.
   */

  /* Step 8: Release JPEG decompression object */

  /* This is an important step since it will release a good deal of memory. */
  jpeg_destroy_decompress(&cinfo);

  /* After finish_decompress, we can close the input file.
   * Here we postpone it until after no more JPEG errors are possible,
   * so as to simplify the setjmp error logic above.  (Actually, I don't
   * think that jpeg_destroy can do an error exit, but why assume anything...)
   */
  fclose(infile);

  /* At this point you may want to check to see whether any corrupt-data
   * warnings occurred (test whether jerr.pub.num_warnings is nonzero).
   */

  /* And we're done! */
  return 1;
}

static int process_unsolicited_jpeg_stream(int clifd,unsigned int jpglen)
{
#define IODATAMAX 100*1024
	int fp;
	int ret;
	char jpgbuf[IODATAMAX];
	int timeout_cnt=0;
	unsigned int received,left;

	/*ipc picture checking stuff*/
	int fd_jpeg=0;
	char jpeg_filename[256] = {0};
	int ipc_check=(getenv_yesno("ipc_check"));
	static int num=0;
	if(ipc_check){	
		if(num>1000) num=0;
		snprintf(jpeg_filename,256,"/run/ipc_%d.jpg",num++);
		fd_jpeg = open(jpeg_filename,O_CREAT|O_RDWR|O_TRUNC ,0666);
		if(fd_jpeg < 0){
		  perror("create ipc jpeg\n");
		}
	}
	
	received=0;
	left=jpglen-received;
	
	while(received!=jpglen){
		int toread=(left> IODATAMAX)?IODATAMAX:left;
		memset(jpgbuf,0x00,sizeof(jpgbuf));
		ret = recv(clifd,jpgbuf,toread,0);
		if(ret == -1 && EINTR == errno){
	    	  timeout_cnt++;	 
	  	  if(timeout_cnt>5000000) {
		    printf("timeout %s %d\n",__FUNCTION__,__LINE__);
		    break;
	 	  }
	  	  usleep(1);
 	 	  continue;
		}

		if(ret>0){
			received += ret;
			left=jpglen-received;
			if(ipc_check&&(fd_jpeg>0)&&write(fd_jpeg,jpgbuf,ret) < 0){
				perror("write jpgbuf");
			}
		}
	}
	
	if(ipc_check&&(fd_jpeg>0)){
		close(fd_jpeg);
		//start to run jpeg checking
		if(!jpeg_file_checking(jpeg_filename)){
			printf("bad jpeg file[%s] detected\n",jpeg_filename);
		}
	}	
	return 0;
}
static int retrieve_socket_resp(int fd,char* recvbuf,unsigned int expected){
  int ret; 
  int timeout_cnt=0;
  unsigned int received=0,left;
  left=expected-received;

  while(received!=expected){	        
	ret = recv(fd,recvbuf+received,left,0);
	if(ret == -1 && EINTR == errno){
	  timeout_cnt++;	 
	  if(timeout_cnt>1000) {
		printf("timeout %s %d\n",__FUNCTION__,__LINE__);
		break;
	  }
	  usleep(1);
 	  continue;
	}
	if(ret>0) {
		received+=ret;
		left=expected-received;
	}

  }
  if(getenv_noyes("ipc_debug"))
	  dumpbuffer("<<",(const char*)recvbuf,received);
  return received;  
}
static int process_ipc_response(int fd)
{
	st_cmd_header_t sch;
	int ret;

	ret = retrieve_socket_resp(fd,(char*)&sch,sizeof(st_cmd_header_t));
	if(ret!=sizeof(st_cmd_header_t)){
		printf("bad response packet %d\n",__LINE__);
		return -1;
	}
	
	if(0x55AA!=sch.flag||0x01!=sch.version){
		printf("unrecognized response packet %d\n",__LINE__);		
		return -1;		
	}
	 if(getenv_noyes("ipc_debug"))
		dump_ipc_cmd_resp_packet(&sch);
	//reach here means recv normally
	switch(sch.command){
		case CMD_SET_JPEG_STREAM_RSP:
		{
			//read left packet
			ret = retrieve_socket_resp(fd,(char*)&cmd_xfer_status.resp,sizeof(st_cmd_xfer_resp_t));
			if(sizeof(st_cmd_xfer_resp_t)!=ret){
				printf("bad response packet %d ret=%d\n",__LINE__,ret);
			}else{
				if(CMD_SET_JPEG_STREAM==cmd_xfer_status.handshake_commnad){
					cmd_xfer_status.handshake_state=1;
				}
			}
		}break;
		case CMD_GET_JPEG_STREAM_RSP:
		{
			//read left packet
			ret = retrieve_socket_resp(fd,(char*)&cmd_xfer_status.resp,sizeof(st_cmd_xfer_resp_t));
			if(sizeof(st_cmd_xfer_resp_t)!=ret){
				printf("bad response packet %d ret=%d\n",__LINE__,ret);
			}else{
				//trying to read left jpeg params
				if(0==cmd_xfer_status.resp.ipc_errno){
			  	    retrieve_socket_resp(fd,(char*)&jpeg_stream_params,sizeof(st_jpeg_stream_params));
				}
				if(CMD_GET_JPEG_STREAM==cmd_xfer_status.handshake_commnad){
					cmd_xfer_status.handshake_state=1;
				}
			}
		}break;
		case CMD_SET_MOTION_DETECT_RSP:
		{
			//read left packet
			ret = retrieve_socket_resp(fd,(char*)&cmd_xfer_status.resp,sizeof(st_cmd_xfer_resp_t));
			if(sizeof(st_cmd_xfer_resp_t)!=ret){
				printf("bad response packet %d ret=%d\n",__LINE__,ret);
			}else{
				if(CMD_SET_MOTION_DETECT==cmd_xfer_status.handshake_commnad){
					cmd_xfer_status.handshake_state=1;
				}
			}
		}break;
		case CMD_GET_MOTION_DETECT_RSP:
		{
			//read left packet
			ret = retrieve_socket_resp(fd,(char*)&cmd_xfer_status.resp,sizeof(st_cmd_xfer_resp_t));
			if(sizeof(st_cmd_xfer_resp_t)!=ret){
				printf("bad response packet %d ret=%d\n",__LINE__,ret);
			}else{
				//trying to read left jpeg params
				if(0==cmd_xfer_status.resp.ipc_errno){
				    retrieve_socket_resp(fd,(char*)&motion_detect_params,sizeof(st_motion_detect_params));
				}
				if(CMD_GET_MOTION_DETECT==cmd_xfer_status.handshake_commnad){
					cmd_xfer_status.handshake_state=1;
				}
			}
		}break;
		case CMD_START_JPEG_STREAM_RSP:
		{
			//read left packet
			ret = retrieve_socket_resp(fd,(char*)&cmd_xfer_status.resp,sizeof(st_cmd_xfer_resp_t));
			if(sizeof(st_cmd_xfer_resp_t)!=ret){
				printf("bad response packet %d ret=%d\n",__LINE__,ret);
			}else{
				if(CMD_START_JPEG_STREAM==cmd_xfer_status.handshake_commnad){
					cmd_xfer_status.handshake_state=1;
				}
			}
		}break;
		case CMD_STOP_JPEG_STREAM_RSP:
		{
			//read left packet
			ret = retrieve_socket_resp(fd,(char*)&cmd_xfer_status.resp,sizeof(st_cmd_xfer_resp_t));
			if(sizeof(st_cmd_xfer_resp_t)!=ret){
				printf("bad response packet %d ret=%d\n",__LINE__,ret);
			}else{
				if(CMD_STOP_JPEG_STREAM==cmd_xfer_status.handshake_commnad){
					cmd_xfer_status.handshake_state=1;
				}
			}
		}break;		
		case CMD_UNSOLICITED_MOTION_WARN:
		{
			//no more payload need to read
			printf("motion detected warning event\n");
		}break;
		case CMD_UNSOLICITED_JPEG_DATA:
		{
			process_unsolicited_jpeg_stream(fd,sch.length);
		}break;
		default:
		{
			printf("unrecognized response command %c\n",sch.command);
		}break;
	}

	
	return 0;
}

static pthread_t tid_ipc_monitor;

static void *ipc_monitor(void *argument)
{
	int stop_monitor=0;
	int clifd=(int)argument;
	int ret;
	fd_set readfds;
	struct timeval tm;

	while(!stop_monitor){
		tm.tv_sec = 3;
		tm.tv_usec = 0;
		FD_ZERO(&readfds);
		FD_SET(clifd,&readfds);
		switch(select(clifd+1,&readfds,NULL,NULL,&tm)){
			case -1:
			{
				printf("select error\n");
				goto monitor_exit;
			}
			case 0:
			{
				//printf("timeout %s\n",__FUNCTION__);
				break;
			}
			default:
			{
				if(FD_ISSET(clifd,&readfds))
					process_ipc_response(clifd);
				break;
			}
		}
	}

monitor_exit:
	return NULL;
	
}
static void dump_jpeg_stream_params(pst_jpeg_stream_params pjsp)
{
	const char* types[]=
	{
		"unknown",
		"jpeg"
	};
	const char* sizes[]=
	{	
		"0x00 :QCIF 176 X 144",
		"0x01 : 180p 320 X 180",
		"0x02 : QVGA 320 X 240",
		"0x03 : CIF 352 X 288",
		"0x04 : VGA 640 X 480",
		"0x05 : D1 720 X 576",
		"0x06 : 720P 1280 X 720",
		"0x07 : 960P�� 1280 X 960",
		"0x08 : UVGA 1600 X 1200",
		"0x09 : 1080P 1920 X 1080",
		"unknown",
	};
	int jpg_size=(pjsp->size>10)?10:pjsp->size;
	printf("============jpeg stream params==========\n");
	
	printf("type:%s\n",pjsp->type<2?types[pjsp->type]:"unknown");
	printf("size:%s\n",sizes[jpg_size]);	
	printf("fps:%d\n",pjsp->fps);
	printf("quality:%d\n",pjsp->quality);
	printf("========================================\n");
	
}
static void dump_motion_detect_params(pst_motion_detect_params pmdp)
{

	printf("============motion detect params==========\n");	
	printf("%s\n",(pmdp->enabled>0)?"enabled":"disabled");
	if(pmdp->enabled>0)
		printf("threshold:%d\n",pmdp->threshold);
	printf("========================================\n");
	
}

static int do_ipc_jpeg_stream_params(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int ret=0;
	if(socket_fd<0){
		printf("ipc monitor not connected\n");
		goto bail;
	}
	if(argc>1){
		unsigned char size = simple_strtoul(argv[1], NULL, 10);
		unsigned char fps=25;
		unsigned char quality=100;
		if(argc>2)
			fps=simple_strtoul(argv[2], NULL, 10);
		if(argc>3)
			quality=simple_strtoul(argv[3], NULL, 10);
		printf("set size=%d,fps=%d,quality=%d\n",size,fps,quality);
		ret = request_jpeg_stream_params(size,fps,quality);
		if(!ret){
			printf("set jpeg stream params okay\n");
			
		}else{
			printf("failed to set jpeg stream params\n");
		}
	}else{
		//
		printf("retrieve jpeg stream params ...\n");
		ret = retrieve_jpeg_stream_params();
		if(!ret){
			printf("retrieve jpeg stream params ...okay\n");
			dump_jpeg_stream_params(&jpeg_stream_params);
		}else{
			printf("failed to retrieve jpeg stream params\n");
		}
	}
	
bail:
	return 0;
	
}

static int do_ipc_motion_detect_params(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int ret=0;
	if(socket_fd<0){
		printf("ipc monitor not connected\n");
		goto bail;
	}
	if(argc>1){
		unsigned char enabled = simple_strtoul(argv[1], NULL, 10);
		unsigned char threshold=50;
		if(argc>2)
			threshold=simple_strtoul(argv[2], NULL, 10);
		ret = request_motion_detect_params(enabled,threshold);
		if(!ret){
			printf("set motion detect params okay\n");
			printf("%s,threshold=%d\n",(enabled>0)?"enabled":"disabled",threshold);
		}else{
			printf("failed to set motion detect params\n");
		}
	}else{
		//
		printf("retrieve motion detect params ...\n");
		ret = retrieve_motion_detect_params();
		if(!ret){
			dump_motion_detect_params(&motion_detect_params);
		}else{
			printf("failed to retrieve motion detectparams\n");
		}
	}
	
bail:
	return 0;

}


static int do_ipc_connect(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int ret=0;
	int port=0;
	char* target=NULL;
	if(socket_fd>=0){
		printf("ipc monitor already started\n");
		return 0;
	}
	if(argc>1)
		target = argv[1];
	else{
	  char *s = getenvvalue("target");
	  if(s) target=s;
	}
	if(argc>2)
		port = simple_strtoul(argv[2], NULL, 10);
	else{
	     char *s = getenvvalue("port");
   	     if(s) port = simple_strtoul(s, NULL, 10);
	}
	
	ret = connect_to_target(target,port);
	if(ret>=0){
		socket_fd = ret;
		#if 0
		pthread_attr_t attr; 
		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED)
		//start retrieve thread now???		
		ret = pthread_create(&pid_ipc_monitor,&attr,(void *)ipc_monitor,socket_fd);
		#else
		ret = pthread_create(&tid_ipc_monitor,NULL,(void *)ipc_monitor,(void*)socket_fd);
		#endif
		if(0!=ret){
			printf("failed to start ipc monitor\n");
			close(socket_fd);
			socket_fd=-1;
		}
	}
	return 0;
	
}

static int do_ipc_disconnect(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	void *res;
	int s;
	if(socket_fd<0){
		printf("ipc monitor not connected\n");
		return 0;
	}
	s = pthread_cancel(tid_ipc_monitor);
	if(0!=s){
		printf("failed to stop ipc monitor\n");
	}else{
		s = pthread_join(tid_ipc_monitor, &res);
		if(PTHREAD_CANCELED==res)
			printf("ipc monitor was canceled\n");
		else
			printf("ipc monitor wasn't canceled (shouldn't happen!)\n");
		close(socket_fd);
		socket_fd=-1;
	}

	return 0;
}

static int do_ipc_start(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    int ret=0;
    if(socket_fd<0){
	printf("ipc monitor not connected\n");
	return 0;
    }
    ret = request_jpeg_stream_start();
    if(!ret)
      printf("start jpeg stream okay\n");
    else
      printf("start jpeg stream failed,result=%d\n",ret);
    return 0;

}
static int do_ipc_stop(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
 int ret=0;
    if(socket_fd<0){
	printf("ipc monitor not connected\n");
	return 0;
    }
    ret = request_jpeg_stream_stop();
    if(!ret)
      printf("stop jpeg stream okay\n");
    else
      printf("stop jpeg stream failed,result=%d\n",ret);
    return 0;
}
/*
 * New command line interface: "env" command with subcommands
 */
static cmd_tbl_t cmd_ipc_sub[] = {
	U_BOOT_CMD_MKENT(jpg, 5, 0, do_ipc_jpeg_stream_params, "", ""),
	U_BOOT_CMD_MKENT(motion, 3, 0, do_ipc_motion_detect_params, "", ""),
	U_BOOT_CMD_MKENT(connect, 3, 0, do_ipc_connect, "", ""),
	U_BOOT_CMD_MKENT(disconnect, 0, 0, do_ipc_disconnect, "", ""),
	U_BOOT_CMD_MKENT(start, 0, 0, do_ipc_start, "", ""),
	U_BOOT_CMD_MKENT(stop, 0, 0, do_ipc_stop, "", ""),
};


static int do_ipc(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	cmd_tbl_t *cp;
	
	/* drop initial "env" arg */
	argc--;
	argv++;
	if(argc!=0){
		cp = find_cmd_tbl(argv[0], cmd_ipc_sub, ARRAY_SIZE(cmd_ipc_sub));

		if (cp)
			return cp->cmd(cmdtp, flag, argc, argv);
	}
	cmd_usage(cmdtp);
	return 1;
}

U_BOOT_CMD(
	ipc, CONFIG_SYS_MAXARGS, 1,	do_ipc,
	"ipc operation,please be noticed,any operations must be executed before ipc monitor started",
	"\njpg [format] [fps] [quality]	-get or set jpeg params\n"
	"      supported formats:                  \n"	
	"                       0x00 :QCIF 176 X 144\n"
	"                       0x01 : 180p 320 X 180\n"
	"                       0x02 : QVGA 320 X 240\n"
	"                       0x03 : CIF 352 X 288\n"
	"                       0x04 : VGA 640 X 480\n"
	"                       0x05 : D1 720 X 576\n"
	"                       0x06 : 720P 1280 X 720\n"
	"                       0x07 : 960P�� 1280 X 960\n"
	"                       0x08 : UVGA 1600 X 1200\n"
	"                       0x09 : 1080P 1920 X 1080\n"
	"                       if capability of device is 960P in maximum, \n"
	"                       if requested size is greater than it,it will be degrade to 960P\n"
	"      supported fps:  1-30                         \n"
	"      supported quality:  0-100,100 is highest quality\n"
	"motion	[enabled] [threshold]   -get or set motion detect params\n"
	"                enabled:  1->enabled 0-->disable\n"	
	"              threshold:  0-100, 100 means highest sensitivity\n"		
	"connect [target] [port]			-start ipc monitor for target\n"	
	"                target: ip address or host name,if no specified,it's \"192.168.42.118\" in default\n"
	"                port: specified port,default is 8008\n"	
	"                           -both target and port support env based setting,$target nad $port\n"
	"disconnect                 -stop ipc monitor\n"
	"start                         -start jpeg stream xfer from target\n"
	"stop                          -stop jpeg stream xfer from target\n"
	"\n\nSupported environment list:\n"
	"                              target=??? -->ip address or host name,\"192.168.42.118\" in default\n"
	"                              port=??? -->specified port,default is 8008\n"
	"                              port=??? -->specified port,default is 8008\n"
	"                              ipc_debug=[y|n] -->dump ipc debugging messages or not,default is 'n',set it to 'y' to enable\n"
	"                              ipc_check=[y|n] -->enable ipc picture integraty checking.\n"	
);



