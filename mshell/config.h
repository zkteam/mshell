#ifndef _CONFIG_H
#define _CONFIG_H

#define CONFIG_VERSION	"mcushell(1.0.0)"
#define CONFIG_SYS_LONGHELP
#define CONFIG_AUTO_COMPLETE
#define CONFIG_CMDLINE_EDITING
#define CONFIG_CMD_ECHO

#define CONFIG_SYS_CBSIZE 1024
#define CONFIG_SYS_MAXARGS	16		/* max number of command args	*/
#define CONFIG_ENV_ADDR 0x0
#define CONFIG_ENV_SIZE	32*1024
#define CONFIG_ETHADDR 00:17:61:00:00:02


#define CONFIG_DEFAULT_ENVS \
"version="CONFIG_VERSION"\0" \
"software=mshell\0"

#define CONFIG_SYS_HUSH_PARSER		1
#define CONFIG_SYS_PROMPT_HUSH_PS2	"> "
#ifdef CONFIG_SYS_HUSH_PARSER
#define CONFIG_SYS_PROMPT		"$ "		/* Monitor Command Prompt */
#define CONFIG_CMD_AUTOSCRIPT
#else
#define CONFIG_SYS_PROMPT		"# "		/* Monitor Command Prompt */
#endif


#endif /*_CONFIG_H*/

