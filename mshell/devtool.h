#ifndef _DEVTOOL_H_
#define _DEVTOOL_H_ 1

/*
 * Sample Usage:
 * 
   int configsize;
   char* configbuffer;
   int ret=devtool_init(DEVTOOL_MODE_DEFAULT);
   if(ret) return ret;

   //allocate buffer for provisioned config
   configbuffer = malloc(configsize=devtool_config_size());
   if(!configbuffer){
       devtool_dispose();  
   	return -ENOMEM;
   }

   //get all configs ,format is
   //"ethaddr=00:17:61:00:00:02\nmodel=p200"
   //
   ret=devtool_config_get(configbuffer,configsize);
   if(ret){
     free(configbuffer);
     devtool_dispose();  
     return ret;
   }

   free(configbuffer);

   //provision config
   ret = devtool_config_provision("model","201");
   if(ret) {
	   devtool_dispose();  
	   return ret;
   }
   
   //provision splash
   ret = devtool_splash_provision("/sdcard/splash.bmp");
   if(ret) {
	   devtool_dispose();  
	   return ret;
   }

   devtool_dispose();   
   
*/

/*
 * DEVTOOL_MODE_QUIET
 *	default mode,as sample usage.
 * DEVTOOL_MODE_INTERACTIVE 
 *    for user interactive operation,it handle devtool_dispose internally
*/
#define DEVTOOL_MODE_QUIET 0
#define DEVTOOL_MODE_INTERACTIVE 1
#define DEVTOOL_MODE_DEFAULT DEVTOOL_MODE_QUIET

int 
	devtool_init(int mode);
int 
	devtool_config_provision(const char* key,const char* value);
int 
	devtool_config_size(void);
int 
	devtool_config_get(char* configbuffer,int size);


int 
	devtool_bmp_provision(const char* bmpname,const char* file);
int 
	devtool_splash_provision(const char* file);
int 
	devtool_get_uniqueid(char* id,int size);

int 
	devtool_set_property(const char* key,const char* value);

char* 
	devtool_get_property(const char* key);

int devtool_dispose(void);


#endif

