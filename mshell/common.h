#ifndef _COMMON_H_
#define _COMMON_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>
#include <zlib.h>
#include <config.h>
#include <environment.h>


#ifdef	DEBUG
#define debug(fmt,args...)	printf (fmt ,##args)
#define debugX(level,fmt,args...) if (DEBUG>=level) printf(fmt,##args);
#else
#define debug(fmt,args...)
#define debugX(level,fmt,args...)
#endif	/* DEBUG */

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
#define ARRAY_AND_SIZE(x)       (x), ARRAY_SIZE(x)


typedef unsigned char uchar;
typedef unsigned long ulong;

typedef signed char s8;
typedef unsigned char u8;

typedef signed short s16;
typedef unsigned short u16;

typedef signed int s32;
typedef unsigned int u32;

typedef signed long long s64;
typedef unsigned long long u64;


typedef void* (*persist_read)(void *dest, const long offset, size_t count);
typedef void (*persist_write)(const long offset, const void *src, size_t count);


typedef struct global_context{
	unsigned long	flags;
	unsigned long	env_addr;	/* Address  of Environment struct */	
	unsigned long	env_valid;	/* Checksum of Environment valid? */

	
	unsigned char	env_data[CONFIG_ENV_SIZE]; /* Environment data		*/
	char		env_buf[32];	/* buffer for getenv() before reloc. */	

	//global persist read/write operation
	persist_read read;
	persist_write write;
	
}gd_t,GLOBAL_CONTEXT_T,*PGLOBAL_CONTEXT_T;

/* * Global Data Flags */
#define	GD_FLG_RELOC		0x00001	/* Code was relocated to RAM		*/
#define	GD_FLG_DEVINIT		0x00002	/* Devices have been initialized	*/
#define	GD_FLG_SILENT		0x00004	/* Silent mode				*/
#define	GD_FLG_POSTFAIL		0x00008	/* Critical POST test failed		*/
#define	GD_FLG_POSTSTOP		0x00010	/* POST seqeunce aborted		*/
#define	GD_FLG_LOGINIT		0x00020	/* Log Buffer has been initialized	*/
#define GD_FLG_DISABLE_CONSOLE	0x00040	/* Disable console (in & out)		*/
#define GD_FLG_ENV_READY	0x00080	/* Environment imported into hash table	*/

extern gd_t* gd;

#define DECLARE_GLOBAL_DATA_PTR     extern gd_t* gd

/*
 * environment porting
*/
int	env_init     (void);
void env_relocate (void);
int	envmatch     (uchar *, int);
char	*getenvvalue(char *);
int	getenv_f     (char *name, char *buf, unsigned len);
int	saveenv	     (void);
int	setenvvalue(char *, char *);
#ifdef CONFIG_AUTO_COMPLETE
int env_complete(char *var, int maxv, char *cmdv[], int maxsz, char *buf);
#endif
int get_env_id (void);

/*
 * utils porting
*/
ulong	simple_strtoul(const char *cp,char **endp,unsigned int base);
unsigned long long	simple_strtoull(const char *cp,char **endp,unsigned int base);
long	simple_strtol(const char *cp,char **endp,unsigned int base);
void dumpbuffer(const char *prefix,const char *data ,int size);

int readline (const char *const prompt);
int getenv_yesno (char *var);
int getenv_noyes (char *var);


int	ctrlc (void);
int	had_ctrlc (void);	/* have we had a Control-C since last clear? */
void	clear_ctrlc (void);	/* clear the Control-C condition */
int	disable_ctrlc (int);	/* 1 to disable, 0 to enable Control-C detect */

#endif
