#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <common.h>
#include <command.h>

int main(int argc,char** argv){
	env_init();	
	printf("\n\n");
	printf("%s\n"
		"type ? or help for command details.\n",CONFIG_VERSION
		);			
	printf("\n\n");
	cmd_loop();
	return 0;
}
