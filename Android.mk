LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
        $(LOCAL_PATH)/devtool \

LOCAL_SHARED_LIBRARIES := 

LOCAL_CFLAGS := -DUSE_HOSTCC -O2 -Wall -Wno-unused-parameter

LOCAL_SRC_FILES:= \
		devtool/flashmanager.c \
		devtool/bmpmanager.c \
		devtool/bmpstub.c \
		devtool/devtool.c \
		devtool/common.c \
		devtool/command.c \
		devtool/env_persist.c \
		devtool/env_common.c \
		devtool/cmd_nvedit.c \
		devtool/hashtable.c \
		devtool/crc32.c \
		devtool/structstorage.c
                
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE:= libdevtool
LOCAL_PRELINK_MODULE := false


include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)

	
LOCAL_SRC_FILES := \
	main.c

LOCAL_C_INCLUDES := \
        $(LOCAL_PATH)/devtool \
    
LOCAL_CFLAGS :=  
LOCAL_SHARED_LIBRARIES := \
	libdevtool

LOCAL_MODULE := devtool
LOCAL_MODULE_TAGS := optional
include $(BUILD_EXECUTABLE)

