# mshell, a shell for easy unit testing

## what is mshell?
    mshell is a tool for automating scripts

## How to build mshell?
     if you want to build mshell,
     First,you shuold set a cmd like :
     
     #define U_BOOT_CMD(name,maxargs,rep,cmd,usage,help) \
     cmd_tbl_t __u_boot_cmd_##name Struct_Section = {#name,                     maxargs, rep, cmd, usage, help}
     
    Then write the do_cmd function you need,after compile you can do your cmd like this:
```      
mcushell(1.0.0)
type ? or help for command details.


$ help

?       - alias for 'help'

autoscr - autoscr - run script from file

echo    - echo    - echo args to console

env     - environment handling commands

exit    - quit current command environment

false   - do nothing, unsuccessfully

help    - print command description/usage

mcu     - mcu --send all mcu cmd to the mcu,then you will know the features mcu support

printenv- print environment variables

quit    - quit current command environment

saveenv - save environment variables to persistent storage

setenv  - set environment variables

showvar - showvar- print local hushshell variables

test    - test    - minimal test like /bin/sh

true    - do nothing, successfully

mcushell(1.0.0)
type ? or help for command details.


$ mcu
mcu - mcu related commands,with this we can detect and check mcu features supported

Usage:
mcu 
ver                             -access mcu firmware version
readtime                        -access mcu time
settime                         -input mcu settime year month day hour minute second to set mcu time
lcd                             -access mcu lenth,wight
pow                             -Check the power supply status
door                            -Check the disassemble alarm switch state and door sensor signal input status
bat                             -Battery charge status value,only for iclock 700
ttys                            -access mcu usb coms
d_sen                           -access door_sensor
hid_wg                          -set HID wiegand

$ mcu ver
generated mcu packet
000000: 53 53 14 eb 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00                                 SS....................
version :20

```


## Technical Support
Please contact icy.liu@zkteco.com raymond.wang@zkteco.com

